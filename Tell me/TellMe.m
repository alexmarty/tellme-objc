//
//  TellMe.m
//  Tell me
//
//  Created by Alexander Martynenko on 11.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import "TellMe.h"

#define kPredictionCount @"predictionCount"

#define kDefaultScreenState @"default"
#define kDefaultTextKey @"Tap to get answer"

#define kNoPredictionScreenState @"no_prediction"
#define kNoPredictionTextKey @"Wait a little and the app will restore its strength"

#define kResetPredictionSceduledDate @"resetPredictionSceduledDate"

#define kLocalNotificationTimeInterval 60*60
#define kChangeStateTimeInterval 10

@interface TellMe ()
@property (nonatomic, strong) NSDictionary *answers;
@property (nonatomic, strong) NSDictionary *screenConfigurations;

@property (nonatomic, strong) NSTimer *timer;
@end

@implementation TellMe

@synthesize textKey = _textKey;
@synthesize screenState = _screenState;
@synthesize predictionCount = _predictionCount;

+ (TellMe *)sharedTellMe
{
    static TellMe *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TellMe alloc] init];
    });
    return sharedInstance;
}

- (NSDictionary *)answers
{
    if (!_answers) {
        NSString *path = [[NSBundle mainBundle] pathForResource: @"Answers" ofType: @"plist"];
        _answers = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    return _answers;
}

- (NSDictionary *)screenConfigurations
{
    if (!_screenConfigurations) {
        NSString *path = [[NSBundle mainBundle] pathForResource: @"ScreenConfigurations" ofType: @"plist"];
        _screenConfigurations = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    return  _screenConfigurations;
}

- (void)setScreenState:(NSString *)screenState
{
    _screenState = screenState;
    [self.delegate tellTe:self didChangeScreenState:screenState];
}

- (NSString *)screenState
{
    if (!_screenState) {
        _screenState = kDefaultScreenState;
    }
    return _screenState;
}

- (void)resetScreenState
{
    if (![self.screenState isEqualToString:kDefaultScreenState]
        && [self.predictionCount integerValue] > 0) {
        self.screenState = kDefaultScreenState;
    } else if (![self.screenState isEqualToString:kNoPredictionScreenState]
               && [self.predictionCount integerValue] == 0) {
        self.screenState = kNoPredictionScreenState;
    }
}

-(void)setTextKey:(NSString *)textKey
{
    _textKey = textKey;
    [self.delegate tellTe:self didChangeTextKey:textKey];
}

-(NSString *)textKey
{
    if (!_textKey) {
        _textKey = kDefaultTextKey;
    }
    return _textKey;
}

- (void)resetTextKey
{
    if (![self.textKey isEqualToString:kDefaultTextKey]
        && [self.predictionCount integerValue] > 0) {
        self.textKey = kDefaultTextKey;
    } else if (![self.textKey isEqualToString:kNoPredictionTextKey]
              && [self.predictionCount integerValue] == 0) {
        self.textKey = kNoPredictionTextKey;
    }
}

- (NSString *)defaultTextKey
{
    return kDefaultTextKey;
}

- (NSString *)noPredictionTextKey
{
    return kNoPredictionTextKey;
}

- (void)reset
{
    [self resetTextKey];
    [self resetScreenState];
}

- (void)setPredictionCount:(NSNumber *)predictionCount
{
    _predictionCount = predictionCount;
    [self.delegate tellTe:self didChangePredictionCount:predictionCount];
}

- (NSNumber *)predictionCount
{
    if (!_predictionCount) {
        _predictionCount = [[NSUserDefaults standardUserDefaults] objectForKey:kPredictionCount];
        if (!_predictionCount) _predictionCount = self.maxPredictionCount;
    }
    return _predictionCount;
}

- (void)resetPredictionCount
{
    if (![self.predictionCount isEqualToNumber:self.maxPredictionCount]) self.predictionCount = self.maxPredictionCount;
    if ([self.textKey isEqualToString:kNoPredictionTextKey]) [self resetTextKey];
    if ([self.screenState isEqualToString:kNoPredictionScreenState]) [self resetScreenState];
}

- (NSNumber *)maxPredictionCount
{
    return @5;
}

- (void)persistPredictionCount
{
    [[NSUserDefaults standardUserDefaults] setObject:self.predictionCount forKey:kPredictionCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)restorePredictionCount
{
    NSDate *resetPredictionSceduledDate = [[NSUserDefaults standardUserDefaults] objectForKey:kResetPredictionSceduledDate];
    NSDate *currentDate = [NSDate date];
    if (resetPredictionSceduledDate && [[currentDate laterDate:resetPredictionSceduledDate] isEqualToDate:currentDate]) {
        [self resetPredictionCount];
    }
}

- (void)predict
{
    NSArray *answerKeys = [self.answers allKeys];
    
    NSString *nextTextKey = kNoPredictionTextKey;
    if (answerKeys && [self.predictionCount integerValue] > 0) {
        // randomize
        unsigned int count = (unsigned int)answerKeys.count - 1;
        nextTextKey = answerKeys[arc4random_uniform(count)];
        
        // set model state
        self.textKey = nextTextKey;
        self.screenState = self.answers[nextTextKey];
        self.predictionCount = @([self.predictionCount integerValue] - 1);
    } else {
        self.textKey = kNoPredictionTextKey;
        self.screenState = kNoPredictionScreenState;
    }
    [self scheduleLocalNotification];
    [self scheduleChangeState];
}

- (void)scheduleLocalNotification
{
    NSArray *scheduledLocalNotifications =[[UIApplication sharedApplication] scheduledLocalNotifications];
    if (scheduledLocalNotifications.count == 0) {
        UILocalNotification *localNotification = [UILocalNotification new];
        NSDate *resetPredictionSceduledDate = [NSDate dateWithTimeIntervalSinceNow:kLocalNotificationTimeInterval * (1 + arc4random_uniform(7))];
        localNotification.fireDate = resetPredictionSceduledDate;
        localNotification.alertBody = NSLocalizedString(@"New predictions for you", @"");
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        [self persistSceduleDate:resetPredictionSceduledDate];
    }
}

- (void)scheduleChangeState
{
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kChangeStateTimeInterval
                                                  target:self
                                                selector:@selector(sceduledChangeState:)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)sceduledChangeState:(id)sender
{
    [self reset];
}

- (void)persistSceduleDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:kResetPredictionSceduledDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)CGColorsForScreenState:(NSString *)screenState
{
    NSMutableArray *colors = [NSMutableArray new];
    for (NSDictionary *colorDictionary in self.screenConfigurations[screenState][@"colors"]) {
        [colors addObject:(id)[UIColor colorWithRed:((NSNumber *)colorDictionary[@"red"]).floatValue / 255.0
                                              green:((NSNumber *)colorDictionary[@"green"]).floatValue / 255.0
                                               blue:((NSNumber *)colorDictionary[@"blue"]).floatValue / 255.0
                                              alpha:((NSNumber *)colorDictionary[@"alpha"]).floatValue / 255.0].CGColor];
    }
    
    return colors;
}

- (NSArray *)locationsForScreenState:(NSString *)screenState
{
    return self.screenConfigurations[screenState][@"locations"];
}

@end
