//
//  ViewController.m
//  Tell me
//
//  Created by Alexander Martynenko on 08.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import "ViewController.h"

#import "TellMe.h"

#import "RadialGradientLayer.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"

#import "Flurry.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "YandexMobileMetrica.h"
#import "AppsFlyerTracker.h"

#define kHelpInfoTimeInterval 4
#define kHelpInfoShowTimeInterval 2
#define kHelpInfoTextKey @"Long press to share"

#define kNewTextKey @"newTextKey"
#define kLastTextKey @"lastTextKey"

@interface ViewController () <TellMeDelegate>
@property (nonatomic, strong) TellMe *tellme;

@property (nonatomic, strong) CAGradientLayer *gradient;
@property (nonatomic, strong) RadialGradientLayer *radialGradient;

@property (nonatomic, strong) MDRadialProgressView * radialProgressView;

@property (nonatomic, strong) UILabel *answerLabel;

@property (nonatomic) BOOL isAnimating;
@property (nonatomic) CFTimeInterval duration;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // init tellme
    self.tellme = [TellMe sharedTellMe];
    self.tellme.delegate = self;
    
    // setup animation
    self.isAnimating = NO;
    self.duration = 1.0;
    
    // setup gradient layer
    self.radialGradient = [RadialGradientLayer layer];
    self.radialGradient.frame = self.view.bounds;
    
    self.radialGradient.startCenter = self.view.center;
    self.radialGradient.endCenter = self.view.center;
    
    CGFloat startRadius = 115.0;
    self.radialGradient.startRadius = startRadius;
    self.radialGradient.endRadius = sqrt(self.view.bounds.size.width*self.view.bounds.size.width/4 + self.view.bounds.size.height*self.view.bounds.size.height/4);
    
    self.radialGradient.colors = [self.tellme CGColorsForScreenState:self.tellme.screenState];
    self.radialGradient.locations = [self.tellme locationsForScreenState:self.tellme.screenState];
    
    id toColor = self.radialGradient.colors[0];
    self.radialGradient.backgroundColor = (__bridge CGColorRef)toColor;
    
    [self.view.layer insertSublayer:self.radialGradient atIndex:0];
    
    // radial progress view
    
    // config radial progress view
    MDRadialProgressTheme *progressTheme = [[MDRadialProgressTheme alloc] init];
    progressTheme.completedColor = [UIColor whiteColor];
    progressTheme.incompletedColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    progressTheme.centerColor = [UIColor clearColor];
    progressTheme.sliceDividerColor = [UIColor clearColor];
    progressTheme.thickness = 8.0;
    progressTheme.labelHidden = YES;
    
    self.radialProgressView = [[MDRadialProgressView alloc] initWithFrame:CGRectMake(0.0, 0.0, startRadius*2, startRadius*2) andTheme:progressTheme];
    self.radialProgressView.center = self.view.center;
    self.radialProgressView.progressTotal = [self.tellme.maxPredictionCount unsignedIntegerValue];
    self.radialProgressView.progressCounter = [self.tellme.predictionCount unsignedIntegerValue];
    [self.view addSubview:self.radialProgressView];
    
    //setup label
    self.answerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 100.0)];
    self.answerLabel.center = self.view.center;
    self.answerLabel.textColor = [UIColor whiteColor];
    self.answerLabel.font = [UIFont systemFontOfSize:19.0];
    self.answerLabel.textAlignment = NSTextAlignmentCenter;
    self.answerLabel.numberOfLines = 0;
    self.answerLabel.text = NSLocalizedString(self.tellme.textKey, @"");
    [self.view addSubview:self.answerLabel];
    
    // tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.view addGestureRecognizer:tap];
    
    // long press
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    longPress.minimumPressDuration = 0.5;
    longPress.allowableMovement = 40.0;
    [self.view addGestureRecognizer:longPress];
    
    
    // for screenshoots
//    [self doScreens];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Main Screen"
                                                      forKey:kGAIScreenName] build]];
}

- (void)doScreens
{
    [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(doScreenshoot:) userInfo:@"Without a doubt" repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:6.0 target:self selector:@selector(doScreenshoot:) userInfo:@"As I see it, yes" repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(doScreenshoot:) userInfo:@"Ask again later" repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(doScreenshoot:) userInfo:@"My reply is no" repeats:NO];
}

- (void)doScreenshoot:(NSTimer *)timer
{
    NSDictionary *textKeys = @{@"Tap to get answer": @5, @"Without a doubt":@4, @"As I see it, yes":@3, @"Ask again later":@2, @"My reply is no":@1};
    
    NSString *textKey = timer.userInfo;
    
    NSString *path = [[NSBundle mainBundle] pathForResource: @"Answers" ofType: @"plist"];
    NSDictionary *screens = [NSDictionary dictionaryWithContentsOfFile:path];
    NSString *screenState = screens[textKey];
    
    self.answerLabel.text = NSLocalizedString(textKey, @"");
    
    self.radialGradient.colors = [self.tellme CGColorsForScreenState:screenState];
    self.radialGradient.locations = [self.tellme locationsForScreenState:screenState];
    id toColor = self.radialGradient.colors[0];
    self.radialGradient.backgroundColor = (__bridge CGColorRef)toColor;
    [self.radialGradient setNeedsDisplay];
    
    self.radialProgressView.progressCounter = [textKeys[textKey] unsignedIntegerValue];
    
}

- (void)tap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (!self.isAnimating) {
        self.isAnimating = YES;
        [self.tellme predict];
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)longPressGestureRecognizer
{
    if (!([self.tellme.textKey isEqualToString:[self.tellme defaultTextKey]]
          || [self.tellme.textKey isEqualToString:[self.tellme noPredictionTextKey]])) {
        NSString *postText = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"TellMe said", @""), NSLocalizedString(self.tellme.textKey, @"")];
        
        UIActivityViewControllerCompletionHandler completionHandler = ^(NSString *activityType, BOOL completed){
            if (completed) {
                [self logShareWithActivityType:activityType];
            }
        };
        UIActivityViewControllerCompletionWithItemsHandler completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            if (completed) {
                [self logShareWithActivityType:activityType];
            }
        };
        
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[postText] applicationActivities:nil];
        
        if ([activityViewController respondsToSelector:@selector(setCompletionHandler:)])
        {
            [activityViewController setCompletionHandler:completionHandler];
        } else {
            [activityViewController setCompletionWithItemsHandler:completionWithItemsHandler];
        }
        
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
}

- (void)logShareWithActivityType:(NSString *)activityType
{
    static NSString *logEvent = @"ActivityVC";

    [Flurry logEvent:logEvent withParameters:@{@"activityType":activityType}];
    
    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createSocialWithNetwork:activityType action:@"share" target:nil] build]];
    
    NSString *yandexLogEvent = [NSString stringWithFormat:@"%@ : %@", logEvent, activityType];
    [YMMCounter reportEvent:yandexLogEvent failure:nil];
    
    [[AppsFlyerTracker sharedTracker] trackEvent:logEvent withValue:activityType];
}

- (void)changeBackgroundForScreenState:(NSString *)screenState
{
    NSArray *fromColors = self.radialGradient.colors;
    NSArray *toColors = [self.tellme CGColorsForScreenState:screenState];
    
    self.radialGradient.colors = toColors;
    
    CABasicAnimation *gradientAnimation = [CABasicAnimation animation];
    
    gradientAnimation.fromValue             = fromColors;
    gradientAnimation.toValue               = toColors;
    gradientAnimation.duration              = self.duration;
    gradientAnimation.removedOnCompletion   = YES;
    gradientAnimation.fillMode              = kCAFillModeForwards;
    gradientAnimation.timingFunction        = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    gradientAnimation.delegate              = self;
    
    id fromColor = fromColors[0];
    id toColor = toColors[0];
    self.radialGradient.backgroundColor = (__bridge CGColorRef)toColor;
    
    CABasicAnimation *backgroundAnimation = [CABasicAnimation animation];
    
    backgroundAnimation.fromValue             = fromColor;
    backgroundAnimation.toValue               = toColor;
    backgroundAnimation.duration              = self.duration;
    backgroundAnimation.removedOnCompletion   = YES;
    backgroundAnimation.fillMode              = kCAFillModeForwards;
    backgroundAnimation.timingFunction        = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    backgroundAnimation.delegate              = self;
    
    // Add the animation to layer
    
    [self.radialGradient addAnimation:gradientAnimation forKey:@"colors"];
    [self.radialGradient addAnimation:backgroundAnimation forKey:@"backgroundColor"];
}

- (void)changeTextLabelForTextKey:(NSString *)textKey
{
    // fade out
    [UIView animateWithDuration:self.duration/2 animations:^{
        self.answerLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        
        // set new answer
        self.answerLabel.text = NSLocalizedString(textKey, @"");
        
        // fade in
        [UIView animateWithDuration:self.duration/2 animations:^{
            if (![textKey isEqualToString:kHelpInfoTextKey])
                self.answerLabel.alpha = 1.0;
            else
                self.answerLabel.alpha = 0.5;
        }];
    }];
}

- (void)changePradictionCount:(NSNumber *)predictionCount
{
    self.isAnimating = YES;
    [UIView animateWithDuration:self.duration/2.0 animations:^{
        
        self.radialProgressView.alpha = 0.5;
        self.radialProgressView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        
    } completion:^(BOOL finished) {
        
        self.radialProgressView.progressCounter = [predictionCount unsignedIntegerValue];
        
        [UIView animateWithDuration:self.duration/2.0 animations:^{
            self.radialProgressView.alpha = 1.0;
            self.radialProgressView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            self.isAnimating = NO;
        }];
    }];
}

#pragma mark - TellMe delegate

- (void)tellTe:(TellMe *)tellme didChangePredictionCount:(NSNumber *)predictionCount
{
    [self changePradictionCount:predictionCount];
    
}

- (void)tellTe:(TellMe *)tellme didChangeScreenState:(NSString *)screenState
{
    [self changeBackgroundForScreenState:screenState];
}

-(void)tellTe:(TellMe *)tellme didChangeTextKey:(NSString *)textKey
{
    //schedile share info
    if (!([textKey isEqualToString:self.tellme.defaultTextKey] ||
          [textKey isEqualToString:self.tellme.noPredictionTextKey]))
    {
        [self scheduleHelpInfoForTextKey:textKey];
    }
    
    [self changeTextLabelForTextKey:textKey];
}

#pragma mark - schedule help info

- (void)scheduleHelpInfoForTextKey:(NSString *)textKey
{
    [self.timer invalidate];
    if (!([textKey isEqualToString:[self.tellme defaultTextKey]]
        || [textKey isEqualToString:[self.tellme noPredictionTextKey]])) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:kHelpInfoTimeInterval
                                                      target:self
                                                    selector:@selector(scheduledHelpInfo:)
                                                    userInfo:@{kLastTextKey:textKey,
                                                               kNewTextKey:kHelpInfoTextKey}
                                                     repeats:NO];
    }
}

- (void)scheduledHelpInfo:(NSTimer *)timer
{
    NSDictionary *userInfo = (NSDictionary *)timer.userInfo;
    [self changeTextLabelForTextKey:userInfo[kNewTextKey]];
    if (userInfo[kLastTextKey]) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:kHelpInfoShowTimeInterval
                                                      target:self
                                                    selector:@selector(scheduledHelpInfo:)
                                                    userInfo:@{kNewTextKey:userInfo[kLastTextKey]}
                                                     repeats:NO];
    }
}

@end
