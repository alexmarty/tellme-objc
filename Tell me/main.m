//
//  main.m
//  Tell me
//
//  Created by Alexander Martynenko on 08.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
