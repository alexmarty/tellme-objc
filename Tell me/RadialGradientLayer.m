//
//  RadialGradientLayer.m
//  Tell me
//
//  Created by Alexander Martynenko on 09.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

struct RadialGradientLayerProperties
{
    __unsafe_unretained NSString *colors;
    __unsafe_unretained NSString *locations;
    
    __unsafe_unretained NSString *startCenter;
    __unsafe_unretained NSString *startRadius;
    __unsafe_unretained NSString *endCenter;
    __unsafe_unretained NSString *endRadius;
};

const struct RadialGradientLayerProperties RadialGradientLayerProperties = {
    .colors = @"colors",
    .locations = @"locations",
    
	.startCenter = @"startCenter",
    .startRadius = @"startRadius",
    .endCenter = @"endCenter",
    .endRadius = @"endRadius",
};

#import "RadialGradientLayer.h"

@implementation RadialGradientLayer

@dynamic colors;
@dynamic locations;

@dynamic startCenter;
@dynamic startRadius;
@dynamic endCenter;
@dynamic endRadius;

-(id)initWithLayer:(id)layer {
    if( ( self = [super initWithLayer:layer] ) ) {
        if ([layer isKindOfClass:[RadialGradientLayer class]]) {
            self.colors = ((RadialGradientLayer*)layer).colors;
            self.startCenter = ((RadialGradientLayer*)layer).startCenter;
            self.startRadius = ((RadialGradientLayer*)layer).startRadius;
            self.endCenter = ((RadialGradientLayer*)layer).endCenter;
            self.endRadius = ((RadialGradientLayer*)layer).endRadius;
        }
    }
    return self;
}

+(BOOL)needsDisplayForKey:(NSString *)key
{
    if ([key isEqualToString:RadialGradientLayerProperties.colors]
        || [key isEqualToString:RadialGradientLayerProperties.locations]
        || [key isEqualToString:RadialGradientLayerProperties.startCenter]
        || [key isEqualToString:RadialGradientLayerProperties.startRadius]
        || [key isEqualToString:RadialGradientLayerProperties.endCenter]
        || [key isEqualToString:RadialGradientLayerProperties.endRadius])
    {
        return YES;
    }
    return [super needsDisplayForKey:key];
}

-(id<CAAction>)actionForKey:(NSString *)event
{
    if ([event isEqualToString:RadialGradientLayerProperties.colors]
        || [event isEqualToString:RadialGradientLayerProperties.locations]
        || [event isEqualToString:RadialGradientLayerProperties.startCenter]
        || [event isEqualToString:RadialGradientLayerProperties.startRadius]
        || [event isEqualToString:RadialGradientLayerProperties.endCenter]
        || [event isEqualToString:RadialGradientLayerProperties.endRadius])
    {
        CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:event];
        theAnimation.fromValue = [self.presentationLayer valueForKey:event];
        return theAnimation;
    }
    return [super actionForKey:event];
}

-(void)drawInContext:(CGContextRef)ctx
{
    NSInteger numberOfLocations = self.locations.count;
    NSInteger numbOfComponents = 0;
    CGColorSpaceRef colorSpace = NULL;
    
    if (self.colors.count) {
        CGColorRef colorRef = (__bridge CGColorRef)[self.colors objectAtIndex:0];
        numbOfComponents = CGColorGetNumberOfComponents(colorRef);
        colorSpace = CGColorGetColorSpace(colorRef);
    }
    
    CGFloat gradientLocations[numberOfLocations];
    CGFloat gradientComponents[numberOfLocations * numbOfComponents];
    
    for (NSInteger locationIndex = 0; locationIndex < numberOfLocations; locationIndex++) {
        
        gradientLocations[locationIndex] = [self.locations[locationIndex] floatValue];
        const CGFloat *colorComponents = CGColorGetComponents((__bridge CGColorRef)self.colors[locationIndex]);
        
        for (NSInteger componentIndex = 0; componentIndex < numbOfComponents; componentIndex++) {
            gradientComponents[numbOfComponents * locationIndex + componentIndex] = colorComponents[componentIndex];
        }
    }
    
    CGGradientRef gradientRef = CGGradientCreateWithColorComponents(colorSpace, gradientComponents, gradientLocations, numberOfLocations);
    CGContextDrawRadialGradient(ctx, gradientRef, self.startCenter, self.startRadius, self.endCenter, self.endRadius, 0);
    
    // release
    CGGradientRelease(gradientRef);
}

@end
