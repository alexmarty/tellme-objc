//
//  RadialGradientLayer.h
//  Tell me
//
//  Created by Alexander Martynenko on 09.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface RadialGradientLayer : CALayer

@property (nonatomic, copy) NSArray *colors;
@property (nonatomic, copy) NSArray *locations;

@property (nonatomic, assign) CGPoint startCenter;
@property (nonatomic, assign) CGFloat startRadius;
@property (nonatomic, assign) CGPoint endCenter;
@property (nonatomic, assign) CGFloat endRadius;

@end
