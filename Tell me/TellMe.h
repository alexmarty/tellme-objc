//
//  TellMe.h
//  Tell me
//
//  Created by Alexander Martynenko on 11.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TellMe;

@protocol TellMeDelegate <NSObject>

- (void)tellTe:(TellMe *)tellme didChangeTextKey:(NSString *)textKey;
- (void)tellTe:(TellMe *)tellme didChangeScreenState:(NSString *)screenState;
- (void)tellTe:(TellMe *)tellme didChangePredictionCount:(NSNumber *)predictionCount;

@end


@interface TellMe : NSObject

+ (TellMe *)sharedTellMe;

@property (nonatomic, weak) id <TellMeDelegate> delegate;

// screenState
@property (nonatomic, strong, readonly) NSString *screenState;
- (void)resetScreenState;

// textKey
@property (nonatomic, strong, readonly) NSString *textKey;
- (void)resetTextKey;

- (NSString *)defaultTextKey;
- (NSString *)noPredictionTextKey;

// prediction count
@property (nonatomic, strong, readonly) NSNumber *predictionCount;
@property (nonatomic, strong, readonly) NSNumber *maxPredictionCount;
- (void)resetPredictionCount;
- (void)persistPredictionCount;
- (void)restorePredictionCount;

- (void)predict;

// reset tellme - reset screenState and textKey
- (void)reset;

// for gradient
- (NSArray *)CGColorsForScreenState:(NSString *)screenState;
- (NSArray *)locationsForScreenState:(NSString *)screenState;

@end
