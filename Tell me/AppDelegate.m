//
//  AppDelegate.m
//  Tell me
//
//  Created by Alexander Martynenko on 08.09.14.
//  Copyright (c) 2014 Alexander Martynenko. All rights reserved.
//

#import "AppDelegate.h"

#import "TellMe.h"

#import "Flurry.h"
#import "GAI.h"
#import "YandexMobileMetrica.h"
#import "AppsFlyerTracker.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //Flurry
    [Flurry startSession:@"WH9XWDT37CBYVTZCZQFW"];
    
    
    // Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-54671686-1"];
    
    // Yandex Metrica
    [YMMCounter startWithAPIKey:29655];
    
    // AppsFlyer
    
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"fQSZKqsFteZkbY3M6zBpVR";
    [AppsFlyerTracker sharedTracker].appleAppID = @"921378203";
    
    
    //for screenshots
//    [application setStatusBarHidden:YES];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[TellMe sharedTellMe] persistPredictionCount];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    [[TellMe sharedTellMe] restorePredictionCount];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[TellMe sharedTellMe] persistPredictionCount];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[TellMe sharedTellMe] resetPredictionCount];
}

@end
